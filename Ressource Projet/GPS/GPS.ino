#include <SoftwareSerial.h>

#include <TinyGPS.h>



//Définition des entrées/sorties du port série logiciel

#define RXPIN 4
#define TXPIN 5 

//Définition des objets

TinyGPS gps;
SoftwareSerial PortSerieLogiciel (RXPIN, TXPIN);
void getgps(TinyGPS &gps);


    void setup() 
  
     {
           // Le port série matériel doit aller beaucoup plus vite que le port du GPS afin de pouvoir afficher
           // ses informations sans risquer de perdres des trames
         
         Serial.begin(115200);
         PortSerieLogiciel.begin(4800); 
         
         Serial.println("");
         Serial.println("Geolocalisation du moniteur GPS");
         Serial.println("...attente d'acquisition des satellites... ");
         Serial.println("");
         
     }


    void loop() 
     
     {
           // Attente de réception d'une trame GPS
           // Vérification de sa validité
        while (PortSerieLogiciel.available())
       
       {
          int c = PortSerieLogiciel.read();
          if(gps.encode(c))
         
         {
          getgps(gps);
         }
       }
     } 
     
           // La fonction getgps va permettre l'extraction des données désirées
    void getgps(TinyGPS &gps)
           
     {     
           // Definition des variables désiées
        float latitude,longitude;
           // Lecture des informations correspondantes
        gps.f_get_position(&latitude, &longitude);
           // Affichage des informations
        Serial.print("Latitude/Longitude:");
        Serial.print(latitude,5);
        Serial.print(", ");
        Serial.println(longitude,5);
           
           // Même principe pour la date et l'heure
        int year;
        byte month, day, hour, minute, second, hundredths;
        gps.crack_datetime(&year,&month,&day,&hour,&minute,&second,&hundredths);
           // Affichage de la date et de l'heure
        Serial.print("Date : "); Serial.print(day, DEC); Serial.print("/") ;
        Serial.print(month, DEC); Serial.print("/"); Serial.print(year);
        Serial.print(" Heure : ");
        Serial.print(hour, DEC); Serial.print("/");
        Serial.print(minute, DEC); Serial.print("/");
        Serial.print(second, DEC);
        Serial.print("."); Serial.println(hundredths, DEC);
           // Affichage de l'altitude, du cap et de la vitesse
        Serial.print("Altitude (metres): ");
        Serial.println(gps.f_altitude());
        Serial.print("Cap (degres): ");
        Serial.println(gps.f_course());
        Serial.print("Vitesse (km/h): ");
        Serial.println(gps.f_speed_kmph());
        Serial.println();
     
     }
